﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace ndcclig
{
    public class NdcNotification
    {
        public const UInt32 FLASHW_STOP      = 0;
        public const UInt32 FLASHW_CAPTION   = 1;
        public const UInt32 FLASHW_TRAY      = 2;
        public const UInt32 FLASHW_ALL       = 3;
        public const UInt32 FLASHW_TIMER     = 4;
        public const UInt32 FLASHW_TIMERNOFG = 12;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        [StructLayout(LayoutKind.Sequential)]
        public struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public Int32 dwTimeout;
        }

        public static void notify ()
        {
            if (NdcPlatform.IsWindows ())
            {
                FlashWindow(Process.GetCurrentProcess().MainWindowHandle);
            }
        }

        private static void FlashWindow(IntPtr hWnd)
        {
            FLASHWINFO fInfo = new FLASHWINFO();

            fInfo.cbSize    = Convert.ToUInt32(Marshal.SizeOf(fInfo));
            fInfo.hwnd      = hWnd;
            fInfo.dwFlags   = FLASHW_TRAY | FLASHW_TIMERNOFG;
            fInfo.uCount    = 1;
            fInfo.dwTimeout = 0;

            FlashWindowEx(ref fInfo);
        }
    }
}

