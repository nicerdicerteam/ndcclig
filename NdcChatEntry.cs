﻿using System;
using Gtk;

namespace ndcclig
{
    public class NdcChatEntry : Gtk.TextView
    {
        public delegate void SubmitMessageEventHandler (object sender, string message);
        public event SubmitMessageEventHandler SubmitMessageEvent;

        public NdcChatEntry ()
        {
            this.AcceptsTab       = false;
            this.LeftMargin       = 3;
            this.PixelsAboveLines = 1;
            this.WrapMode         = WrapMode.WordChar;

            this.ShowAll();
        }

        protected override bool OnKeyPressEvent(Gdk.EventKey evnt)
        {
            if ((evnt.Key == Gdk.Key.Return) &&
                ((evnt.State & (Gdk.ModifierType.ControlMask | Gdk.ModifierType.ShiftMask)) == 0))
            {
                SubmitMessageEvent.Invoke(this, this.Buffer.Text);
                this.Buffer.Clear();
                return true;
            }
            else
                return base.OnKeyPressEvent(evnt);
        }
    }
}

