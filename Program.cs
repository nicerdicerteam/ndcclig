﻿using System;
using Gtk;

namespace ndcclig
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Application.Init ();
            NdcClientWindow win = new NdcClientWindow ();
            win.Show ();
            Application.Run ();
        }
    }
}
