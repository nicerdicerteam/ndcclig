﻿using System;
using System.IO;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using Gtk;
using ndc;
using ndcclig;

public partial class NdcClientWindow: Gtk.Window
{
    private string host;
    private string nickname;
    private int port;
    private bool connected;
    private bool toggling;
    private bool stick_to_end;
    private bool notify_new_messages;

    private ListStore messages;
    private ListStore clients;

    private NdcChatEntry ent_ChatMessage;

    private System.Threading.Thread receiver;
    private System.Net.Sockets.Socket server;

    private Menu mnu_MessagesContextMenu;


    /* -------------------------------------------------------------------------------------------------------------- */

    private void init_messages_view (TreeView view)
    {
        /* create columns */
        TreeViewColumn column_sender  = new TreeViewColumn ();
        TreeViewColumn column_message = new TreeViewColumn ();
        column_sender.Title  = "sender";
        column_message.Title = "message";

        /* create cell renderers */
        CellRendererText cell_sender  = new CellRendererText ();
        CellRendererText cell_message = new CellRendererText ();
        column_sender.PackStart (cell_sender, true);
        column_message.PackStart (cell_message, true);
        column_sender.AddAttribute (cell_sender, "text", 0);
        column_message.AddAttribute (cell_message, "text", 1);

        /* add columns to the view */
        view.AppendColumn (column_sender);
        view.AppendColumn (column_message);

        /* hide column headers */
        view.HeadersVisible = false;
    }

    private void init_clients_view (TreeView view)
    {
        /* create columns */
        TreeViewColumn column_ip = new TreeViewColumn ();
        column_ip.Title          = "IP";

        /* create cell renderers */
        CellRendererText cell_ip = new CellRendererText ();
        column_ip.PackStart (cell_ip, true);
        column_ip.AddAttribute (cell_ip, "text", 0);

        /* add columns to the view */
        view.AppendColumn (column_ip);
    }

    private Menu create_messages_context_menu()
    {
        Menu m        = new Menu();
        MenuItem item = new MenuItem ("Copy message");
        item.Activated += OnMenuItemCopyMessageActivated;
        m.Append(item);
        m.ShowAll();

        return m;
    }

    private NdcChatEntry create_chat_message_entry(Container parent_widget)
    {
        NdcChatEntry entry = new NdcChatEntry();
        entry.SubmitMessageEvent += OnChatEntrySubmitMessageEvent;
        parent_widget.Add(entry);

        return entry;
    }

    public NdcClientWindow () : base (Gtk.WindowType.Toplevel)
    {
        stick_to_end = true;

        Build ();

        /* create data stores for chat history and "connected clients"-view */
        messages = new ListStore (typeof(string), typeof(string));
        trv_Messages.Model = messages;
        clients = new ListStore (typeof(string));
        trv_Clients.Model = clients;

        /* create additional widgets */
        mnu_MessagesContextMenu = create_messages_context_menu();
        ent_ChatMessage         = create_chat_message_entry(scroll_ChatMessage);

        /* initialize widgets */
        init_messages_view(trv_Messages);
        init_clients_view(trv_Clients);

        /* set default values */
        this.Title          = "NdcClient GUI v0.2.5";
        ent_Port.Text       = "7000";
        notify_new_messages = chk_NotifyMessages.Active;

        refresh_ui ();
    }


    /* -------------------------------------------------------------------------------------------------------------- */

    private void refresh_ui ()
    {

        btn_Send.Sensitive         = connected;
        ent_Nickname.Sensitive     = !connected;
        ent_Host.Sensitive         = !connected;
        ent_Port.Sensitive         = !connected;
        ent_ChatMessage.Sensitive  = connected;

        if (connected)
        {
            tbtn_Connected.Label = "Disconnect";
            ent_ChatMessage.GrabFocus();
        }
        else
        {
            clients.Clear ();
            tbtn_Connected.Label = "Connect";
            ent_Nickname.GrabFocus();
        }
    }

    private void send_message (string message)
    {
        if (connected)
        {
            NdcMessage m = new NdcMessage (message, this.nickname);
            m.send (this.server);
        }
    }

    private void store_message (string sender, string message)
    {
        /*
         * Updating the the chat messages must happen in the main thread!
         */
        Gtk.Application.Invoke (delegate {
            messages.AppendValues (sender + ">", message);
        });
    }

    private void update_clients_list (string clients_list_string)
    {
        string[] clients_array = clients_list_string.Split (';');
        /*
         * Updating the clients ListStore must happen in the main thread!
         * Therefore use delegate invoke:
         *  - Clear list
         *  - Append all clients
         */
        Gtk.Application.Invoke(delegate {
            clients.Clear();
            for (int i = 0; i < clients_array.Length; i++)
                clients.AppendValues (clients_array [i]);
        });
    }

    private void handle_command (NdcMessage m)
    {
        switch (m.get_command())
        {
            case NdcMessageCommand.NDC_MESSAGE_COMMAND_CLIENT_LIST:
            {
                update_clients_list (m.get_message());
                break;
            }

            default:
                store_message ("ERROR", "unknown command: " + m.get_command().ToString());
                break;
        }
    }

    private void handle_message (NdcMessage m)
    {
        if (m.is_command ())
            handle_command (m);
        else
        {
            store_message (m.get_sender (), m.get_message ());
            notify_new_message ();
        }
    }

    private void notify_new_message ()
    {
        if (notify_new_messages) NdcNotification.notify ();
    }

    private void reset_state()
    {
        Gtk.Application.Invoke(delegate {
            disconnect();
            refresh_ui();

            /* manually toggle the state of the "Connected"-button */
            toggling = true;
            try
            {
                tbtn_Connected.Active = false;
            }
            finally
            {
                toggling = false;
            }
        });
    }

    private void connect (string host, int port, string nickname)
    {
        /* store connection info */
        this.host     = host;
        this.port     = port;
        this.nickname = nickname;

        /* connect to server-socket on default port 7000 */
        server = new System.Net.Sockets.Socket (AddressFamily.InterNetwork, SocketType.Stream, 0);
        server.Connect (this.host, this.port);
        connected = true;

        /* invoke the message receiver thread */
        receiver = new Thread(new ThreadStart(this.receiver_thread_proc));
        receiver.Start ();
    }

    private void disconnect ()
    {
        this.connected = false;

        try
        {
            /* send disconnect message to server */
            NdcMessage exit_message = new NdcMessage (NdcMessageCommand.NDC_MESSAGE_COMMAND_DISCONNECT);
            exit_message.send (this.server);
        }
        catch (SocketException)
        {
            /*
             * The Socket is already closed -> disconnected disgracefully
             * Ignore any Socket Exceptions.
             */
        }

        /* join the message receiver thread */
        this.receiver.Join ();
    }

    private string get_selected_message ()
    {
        TreeModel store;
        TreeIter iter;
        TreeSelection s = trv_Messages.Selection;

        if (s.GetSelected (out store, out iter))
            return (string) store.GetValue (iter, 1);
        else
            return "";
    }

    private void copy_selected_message ()
    {
        string message = get_selected_message ();
        if (message != "")
            Clipboard.Get(null).Text = message;
    }


    /* -------------------------------------------------------------------------------------------------------------- */

    private void OnDeleteEvent (object sender, DeleteEventArgs a)
    {
        if (connected) disconnect ();
        Application.Quit ();
        a.RetVal = true;
    }

    private void OnBtnSendClicked (object sender, EventArgs e)
    {
        this.send_message (this.ent_ChatMessage.Buffer.Text);
        this.ent_ChatMessage.Buffer.Clear();
    }

    private void OnChatEntrySubmitMessageEvent (object sender, string message)
    {
        send_message(message);
    }

    private void OnTbtnConnectedToggled (object sender, EventArgs e)
    {
        /*
         * Do not execute event if we are currently toggling the state of the Connect-button manually
         */
        if (toggling) return;

        try
        {
            if (tbtn_Connected.Active)
                connect (ent_Host.Text, int.Parse(ent_Port.Text), ent_Nickname.Text);
            else
                disconnect ();

            refresh_ui ();
        }
        catch (Exception ex)
        {
            toggling = true;
            try
            {
                tbtn_Connected.Active = !tbtn_Connected.Active;
            }
            finally
            {
                toggling = false;
            }
            store_message ("ERROR", ex.Message);
        }
    }

    protected void OnTrvMessagesSizeAllocated (object o, SizeAllocatedArgs args)
    {
        if (this.stick_to_end)
        {
            /*
             * Scroll to the end of the treeview to display the last messages
             */
            Adjustment adj = trv_Messages.Vadjustment;
            adj.Value = adj.Upper - adj.PageSize;
        }
    }

    protected void OnTrvMessagesKeyPressEvent (object o, KeyPressEventArgs args)
    {
        /*
         * Copy currently selected message to clipboard if [Ctrl]+[C] was pressed
         */
        if (((args.Event.State & Gdk.ModifierType.ControlMask) == Gdk.ModifierType.ControlMask) &&
            (args.Event.Key == Gdk.Key.c))
            copy_selected_message ();
    }

    protected void OnTrvMessagesScrollEvent (object o, ScrollEventArgs args)
    {
        /*
         * When scrollbar is being moved, check if the scrollbar is at the end of the message view.
         * Set the stick-to-end flag accordingly.
         */
        Adjustment adj    = trv_Messages.Vadjustment;
        this.stick_to_end = (adj.Value == adj.Upper - adj.PageSize);
    }

    protected void OnChkNotifyMessagesToggled (object sender, EventArgs e)
    {
        this.notify_new_messages = this.chk_NotifyMessages.Active;
    }

    protected void OnTrvMessagesButtonReleaseEvent (object o, ButtonReleaseEventArgs args)
    {
        /*
         * Show message context menu on right-click
         */
        if (args.Event.Button == 3 &&
            (trv_Messages.Selection.CountSelectedRows() > 0))
            mnu_MessagesContextMenu.Popup ();
    }

    protected void OnMenuItemCopyMessageActivated (object sender, EventArgs e)
    {
        copy_selected_message ();
    }


    /* -------------------------------------------------------------------------------------------------------------- */

    private void receiver_thread_proc ()
    {
        while (connected)
        {
            NdcMessage m = new NdcMessage ();
            try
            {
                if (m.receive (this.server))
                    handle_message(m);
                else
                {
                    /*
                     * Make sure no further messages are tried to be received immediately.
                     * This must be separate from the reset_state()-function to ensure the main-thread is able to join
                     * the receiver thread.
                     */
                    Gtk.Application.Invoke(delegate {
                        connected = false;
                    });
                    reset_state();
                    store_message("WARNING", "Server did not respond. Closing Connection ...");
                }
            }
            catch (Exception ex)
            {
                this.store_message ("ERROR", ex.Message);
            }
        }
    }
}
