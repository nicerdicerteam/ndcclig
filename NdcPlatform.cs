﻿using System;
using System.IO;

namespace ndcclig
{
    public enum NdcPlatformType
    {
        NDC_PLATFORM_LINUX,
        NDC_PLATFORM_MAC,
        NDC_PLATFORM_WINDOWS
    }

    public class NdcPlatform
    {
        private static NdcPlatformType _type;

        static NdcPlatform ()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                        // Well, there are chances MacOSX is reported as Unix instead of MacOSX.
                        // Instead of platform check, we'll do a feature checks (Mac specific root folders)
                    if (Directory.Exists ("/Applications")
                            & Directory.Exists ("/System")
                            & Directory.Exists ("/Users")
                            & Directory.Exists ("/Volumes"))
                        _type = NdcPlatformType.NDC_PLATFORM_MAC;
                    else
                        _type = NdcPlatformType.NDC_PLATFORM_LINUX;
                    break;

                case PlatformID.MacOSX:
                    _type = NdcPlatformType.NDC_PLATFORM_MAC;
                    break;

                default:
                    _type = NdcPlatformType.NDC_PLATFORM_WINDOWS;
                    break;
            }
        }

        public static bool IsLinux ()
        {
            return _type == NdcPlatformType.NDC_PLATFORM_LINUX;
        }

        public static bool IsMac ()
        {
            return _type == NdcPlatformType.NDC_PLATFORM_MAC;
        }

        public static bool IsWindows ()
        {
            return _type == NdcPlatformType.NDC_PLATFORM_WINDOWS;
        }
    }
}

